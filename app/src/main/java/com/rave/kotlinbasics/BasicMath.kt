package com.rave.kotlinbasics

fun calculateSum(firstNumber: Int = 10, secondNumber: Int = 5) {
    //val firstNumber = "10"
    //val secondNumber = "5"
    val result = firstNumber + secondNumber

    println("$firstNumber + $secondNumber = $result")
}