package com.rave.kotlinbasics


fun displayAlert(operatingSystem: String = "Unknown OS", emailId: String){
    println("There's a new sign-in request on $operatingSystem for your Google Account $emailId")
}