package com.rave.kotlinbasics

fun printMethod(city: String, low: Int, high: Int, percentRain: Int){
        println("City: $city\nLow Temperature: $low, High Temperature: $high\nChance of rain: $percentRain%\n")
}